/* code for the "myobj" pd class.  Increment a number.  */

#include "m_pd.h"
 
typedef struct myobj
{
  t_object x_ob;
  t_outlet *x_outlet;
} t_myobj;

void myobj_float(t_myobj *x, t_floatarg f)
{
    outlet_float(x->x_outlet, f + 1.);
}

t_class *myobj_class;

void *myobj_new(void)
{
    t_myobj *x = (t_myobj *)pd_new(myobj_class);
    x->x_outlet = outlet_new(&x->x_ob, gensym("float"));
    return (void *)x;
}

void myobj_setup(void)
{
    myobj_class = class_new(gensym("myobj"), (t_newmethod)myobj_new,
    	0, sizeof(t_myobj), 0, 0);
    class_addfloat(myobj_class, myobj_float);
}

